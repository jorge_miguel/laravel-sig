<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSigDbProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('categoria_id')->nullabel(true);
            $table->string('nombre',255)->nullabel(true);
            $table->text('descripcion')->nullabel(true);
            $table->float('precio',100,2)->nullabel(true);
            $table->integer('stock')->nullabel(true);
            $table->string('oferta',255)->nullabel(true);
            $table->date('fecha')->nullabel(true);
            $table->string('imagen',255)->nullabel(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
