<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSigDbLineasPedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lineas-pedidos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('pedido_id')->nullabel(true);
            $table->integer('producto_id')->nullabel(true);
            $table->integer('unidades')->nullabel(true);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
