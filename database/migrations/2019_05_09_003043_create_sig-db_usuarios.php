<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSigDbUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre')->nullabel(true);
            $table->string('apellidos')->nullabel(true);
            $table->string('email')->nullabel(true);
            $table->string('password')->nullabel(true);
            $table->string('rol')->nullabel(true);
            $table->string('imagen')->nullabel(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
