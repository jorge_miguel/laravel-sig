<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSigDbPedidos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('usuario_id')->nullabel(true);
            $table->string('provincia',255)->nullabel(true);
            $table->string('localidad',255)->nullabel(true);
            $table->string('direccion',100,2)->nullabel(true);
            $table->float('coste',100,2)->nullabel(true);
            $table->string('estado',255)->nullabel(true);
            $table->date('fecha')->nullabel(true);
            $table->time('hora')->nullabel(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
